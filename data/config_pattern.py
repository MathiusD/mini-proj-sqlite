default_delimiter = ";"
name_table = "SIV"
pattern_table = {
    "data":[
        {"name":"adresse_titulaire", "type":"TEXT", "not_null":"true"},
        {"name":"nom", "type":"TEXT", "not_null":"true"},
        {"name":"prenom", "type":"TEXT", "not_null":"true"},
        {"name":"immatriculation", "type":"TEXT", "not_null":"true"},
        {"name":"date_immatriculation", "type":"TEXT", "not_null":"true"},
        {"name":"vin", "type":"INTEGER", "not_null":"true"},
        {"name":"marque", "type":"TEXT", "not_null":"true"},
        {"name":"denomination_commerciale", "type":"TEXT", "not_null":"true"},
        {"name":"couleur", "type":"TEXT", "not_null":"true"},
        {"name":"carrosserie", "type":"TEXT", "not_null":"true"},
        {"name":"categorie", "type":"TEXT", "not_null":"true"},
        {"name":"cylindre", "type":"INTEGER", "not_null":"true"},
        {"name":"energie", "type":"INTEGER", "not_null":"true"},
        {"name":"places", "type":"INTEGER", "not_null":"true"},
        {"name":"poids", "type":"INTEGER", "not_null":"true"},
        {"name":"puissance", "type":"INTEGER", "not_null":"true"},
        {"name":"type", "type":"TEXT", "not_null":"true"},
        {"name":"variante", "type":"TEXT", "not_null":"true"},
        {"name":"version", "type":"INTEGER", "not_null":"true"}
    ],
    "primary_key":{"column":"immatriculation", "Auto_Increment":"false"},
    "foreign_key":[] 
}
pattern_data = {
    "search":{
        "column":"immatriculation",
        "table":"SIV",
        "option":[
            {"column":"immatriculation","data":{"format":"search_input","criteria":""}}
        ],
        "seperator_option":" AND ",
        "type_key":str
    },
    "compare":{
        "column":"*",
        "table":"SIV",
        "option":[
        ],
        "seperator_option":"",
        "type_key":tuple
    },
    "cast_compare":int,
    "list_cast_compare":[
        "vin",
        "cylindre",
        "energie",
        "places",
        "poids",
        "puissance",
        "version"
    ],
    "insert/update":{
        "table":"SIV",
        "data":[
            "adresse_titulaire",
            "nom",
            "prenom",
            "immatriculation",
            "date_immatriculation",
            "vin",
            "marque",
            "denomination_commerciale",
            "couleur",
            "carrosserie",
            "categorie",
            "cylindre",
            "energie",
            "places",
            "poids",
            "puissance",
            "type",
            "variante",
            "version"
        ],
        "string":[
            "adresse_titulaire",
            "nom",
            "prenom",
            "immatriculation",
            "date_immatriculation",
            "marque",
            "denomination_commerciale",
            "couleur",
            "carrosserie",
            "categorie",
            "type",
            "variante",
        ],
        "cp":"immatriculation"
    }
}