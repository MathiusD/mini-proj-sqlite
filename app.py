import argparse, logging, time
from own.own_sqlite.bdd import bdd_exist, bdd_create
from own.own_sqlite.connect import connect
from own.own_sqlite.data import data_exist, data_insert, data_update
from own.own_sqlite.table import table_exist, table_create
from own.own_csv.read_csv import read_csv
from own.own_csv.verif_csv import verif_csv
from data.config_pattern import default_delimiter, pattern_table, pattern_data, name_table
from own.own_logging.logging import return_log_level
from own.own_struct.struct import dict_to_tuple

#Définition des arguments pour appeler notre applicatif depuis la console.
parser = argparse.ArgumentParser(description='For upload CSV File in SQLite DataBase')
parser.add_argument("file", help="csv file to upload")
parser.add_argument("database", help="target database")
parser.add_argument("-delimiteur", help="Delimiteur used (Default : ';')")
parser.add_argument("-log_level", help="Level for Logging (Default : INFO)")
args = parser.parse_args()

if (args.log_level != None):
    args.log_level = return_log_level(args.log_level)
else:
    args.log_level = logging.INFO

date = time.time()
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename=str(date) + '.log', level=args.log_level)

if (args.delimiteur == None):
    args.delimiteur = default_delimiter

logging.info("Verification Database")
if (bdd_exist(args.database) != True):
    logging.info("Creating Database")
    bdd_create(args.database)

logging.info("Connecting Database")
bdd = connect(args.database)

logging.info("Verification Table " + str(name_table))
if (table_exist(name_table, bdd) != True):
    logging.info("Creating Table " + str(name_table))
    table_create(name_table, pattern_table, bdd)

logging.info("Verification File CSV")
if (verif_csv(args.file) == True):
    #Si les arguments sont bon et le fichier aussi on le traite
    logging.info("Read File CSV")
    csv = read_csv(args.file, args.delimiteur)
    indice = 1
    insert = 0
    not_insert = 0
    update = 0
    not_update = 0
    ignored = 0
    length = len(csv)
    for row in csv:
        progress =  "(Data:" + str(indice) + "/" + str(length) + ")"
        logging.debug("Searching Data " + progress)
        if (data_exist(row[pattern_data['search']['column']], pattern_data['search'], bdd) == False):
            logging.debug("Insert Data " + progress)
            if (True == data_insert(row, pattern_data["insert/update"], bdd)):
                logging.debug("Data Inserted " + progress)
                insert = insert + 1
            else:
                logging.warning("Data NOT Inserted " + progress)
                not_insert = not_insert + 1
        else:
            if (data_exist(dict_to_tuple(row, pattern_data['cast_compare'], pattern_data['list_cast_compare']), pattern_data['compare'], bdd, False) == False):
                logging.debug("Update Data " + progress)
                if (True == data_update(row, pattern_data["insert/update"], bdd)):
                    logging.debug("Data Updated " + progress)
                    update = update + 1
                else:
                    logging.warning("Data NOT Updated " + progress)
                    not_update = not_update + 1
            else:
                logging.debug("Data NOT Updated (Data is already up-to-date)" + progress)
                ignored = ignored + 1
        indice = indice + 1
    if insert > 0:
        logging.info(str(insert) + " Data Inserted.")
    if not_insert > 0:
        logging.warning(str(not_insert) + " Data NOT Inserted.")
    if update > 0:
        logging.info(str(update) + " Data Updated.")
    if not_update > 0:
        logging.warning(str(not_update) + " Data NOT Updated.")
    if ignored > 0:
        logging.info(str(ignored) + " Data NOT Updated (Data is already up-to-date).")
    bdd.close()
    logging.info("Execution Finished")
else:
    logging.warning("File Incorrect")
    #Si le fichier est incorrect
    print("File is incorrect")