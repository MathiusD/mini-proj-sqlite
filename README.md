# Mini Projet 'Import dans une base de Données'

## Description

Ce projet est le projet suivant 'Une histoire d'automobile'. Lors de ce projet nous devons à l'aide d'un fichier csv passé en ligne de commande générer ou mettre à jour une base de données. Le but de ce dernier était de nous faire manipuler les bases de données en python ainsi que le logging.

## Auteurs

* Mathieu Féry
* Paul Rosselle
