import sqlite3
connection = sqlite3.connect('boardgames.sqlite3')

cursor = connection.cursor()
cursor.execute('''CREATE TABLE stocks
(date text, trans text, symbol text, qty real, price real)''')

cursor.execute('''INSERT INTO stocks VALUES ('2006-01-05', 'BUY', 'RHAT', 100, 35.14)''')
connection.commit()

values = ('RHAT',)
cursor.execute('SELECT * FROM stocks WHERE symbol=?', values)

purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00)
            ]
cursor.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases)
connection.commit()

cursor.execute('SELECT * FROM stocks WHERE symbol=?', ('RHAT',))
print(cursor.fetchone())
cursor.execute('SELECT * FROM stocks ORDER BY price')
print(cursor.fetchall())
for row in cursor.execute('SELECT * FROM stocks ORDER BY price'):
    print(row)